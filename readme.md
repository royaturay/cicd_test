[![pipeline status](https://gitlab.com/royaturay/cicd_test/badges/master/pipeline.svg)](https://gitlab.com/royaturay/cicd_test/commits/master)
[![coverage report](https://gitlab.com/royaturay/cicd_test/badges/master/coverage.svg)](https://gitlab.com/royaturay/cicd_test/commits/master)

## Ansible & GitLab CI/CD workshop 101

此範例程式碼即是 [PHP Framework Laravel 5.4.*](https://laravel.com/docs/5.4#installation)，如果你熟悉 Laravel，你也可以自行預備。

取得程式碼後，即可依據 Workshop 101 的步驟開始嘗試建立你自己的 CI/CD Pipeline。

* [操作步驟](http://blog.chengweichen.com/2017/08/coscup-2017-ansible-gitlab-cicd.html)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
